import pandas as pd
import sqlalchemy
from binance.client import Client
from binance import BinanceSocketManager
import env

client = Client(env.api_key, env.api_secret)

bsm = BinanceSocketManager(client)
socket = bsm.trade_socket("BTCUSDT")


def createFrame(msg):
    df = pd.DataFrame([msg])
    df = df.loc[:, ["s", "E", "p"]]
    df.columns = ["symbol", "time", "price"]
    df.price = df.price.astype(float)
    df.tim = pd.to_datetime(df.time, units="ms")
    return df


engine = sqlalchemy.create_engine("sqlite:///BTCUSDstream.db")

while True:
    await socket.__aenter__()
    msg = await socket.recv()
    frame = createFrame(msg)
    frame.to_sql("BTCUSDT", engine, if_exists="append", index=False)
    print(msg)
